FROM igwn/base:el9

RUN dnf -y upgrade $$ dnf clean all

COPY extra-packages /
RUN dnf -y install $(<extra-packages) && dnf clean all
RUN rm /extra-packages
